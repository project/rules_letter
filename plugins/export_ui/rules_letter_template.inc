<?php

/**
 * @file
 */

$plugin = array(
	'schema' => 'rules_letter_templates',
  
  'access' => 'administer letter templates',
  
  'menu' => array(
    'menu prefix' => 'admin/config/workflow',
    'menu item' => 'letters',
    'menu title' => 'Letters',
    'menu descriptions' => 'Administer letter templates.',
  ),
  
  'title singular' => t('letter template'),
  'title singular proper' => t('Letter template'),
  'title plural' => t('letter templates'),
  'title plural proper' => t('Letter templates'),
  
  'form' => array(
    'settings' => 'rules_letter_admin_template_form',
  ),
);

function rules_letter_admin_template_form(&$form, &$form_state) {
  
  $form['info']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $form_state['item']->description,
    '#maxlength' => 128,
  );

  $form['info']['fid'] = array(
    '#type' => 'managed_file',
    '#title' => t('PDF form template'),
    '#default_value' => $form_state['item']->fid,
  	'#upload_validators' => array(
      'file_validate_extensions' => array('pdf'),
      'rules_letter_file_validate_has_field' => array(),
  	),
    '#upload_location' => variable_get('rules_letter_template_path', 'private://letters/templates'),
    '#required' => TRUE,
  );
}